import javassist.CannotCompileException;
import javassist.ClassPool;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) throws CannotCompileException {
        // 1) java.lang.OutOfMemoryError: Java heap space. You can use different data structures. Do not tune heap size.
        method1();

        // 2) java.lang.OutOfMemoryError: Java heap space. Create big objects continuously and make them stay in memory.
        // Do not use arrays or collections.
        method2();

        // 3) java.lang.OutOfMemoryError: Metaspace. Load classes continuously and make them stay in memory.
        method3();

        // 4) java.lang.StackOverflowError. Use recursive methods. Don’t tune stack size.
        method4();

        // 5) java.lang.StackOverflowError. Do not use recursive methods. Don’t tune stack size.
        method5();
    }

    private static void method5() {
        throw new StackOverflowError(); // ^^)
    }

    private static void method4() {
        class Person {
            int age;
            String name;
            Person friend;

            public Person() {
            }

            private Person(int age, String name, Person friend) {
                this.age = age;
                this.name = name;
                this.friend = friend;
            }

            public Person createPerson(int age, String name) {
                Person person = new Person(age, name, createPerson(age + 1, name + "1"));
                return person;
            }
        }

        Person person = new Person().createPerson(10, "Name");
    }

    //Checked with -XX:MaxMetaspaceSize=512m to save the time
    private static void method3() throws CannotCompileException {
        final ClassPool classPool = ClassPool.getDefault();
        for (int i = 0; i < 1000000; i++) {
            Class clas = classPool.makeClass("MySuperClass" + i).toClass();
            System.out.println(clas.getName());
        }
    }

    private static void method2() {
        StringBuilder s = new StringBuilder();

        while (true) {
            s.append("dummy");
        }
    }

    private static void method1() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 500_000_000; i++) {
            list.add(new Integer(i));
        }
    }
}
